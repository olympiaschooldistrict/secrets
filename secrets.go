package secrets

import (
	"context"
	"fmt"

	secretmanager "cloud.google.com/go/secretmanager/apiv1"
	secretmanagerpb "google.golang.org/genproto/googleapis/cloud/secretmanager/v1"
)

func MustString(s string, err error) string {
	if err != nil {
		panic(err)
	}
	return s
}
func MustByte(b []byte, err error) []byte {
	if err != nil {
		panic(err)
	}
	return b
}

func GetString(ctx context.Context, name string) (string, error) {
	data, err := GetBytes(ctx, name)
	if err != nil {
		return "", err
	}
	return string(data), err
}
func GetBytes(ctx context.Context, name string) ([]byte, error) {
	c, err := secretmanager.NewClient(ctx)
	if err != nil {
		return nil, err
	}

	req := &secretmanagerpb.AccessSecretVersionRequest{
		Name: name,
	}
	result, err := c.AccessSecretVersion(ctx, req)
	if err != nil {
		return nil, err
	}
	if result != nil {
		if result.Payload != nil {
			return result.Payload.Data, nil
		}
	}
	return nil, fmt.Errorf("Failed to secure the secret")
}
